############################
#      Main Variables      #
############################

variable "env" {
  description = "Environment"
  type        = string
  default     = ""
}

variable "nomad_datacenter" {
  description = "Nomad Datacenter"
  type        = string
  default     = ""
}

variable "nomad_node_class" {
  description = "Nomad Node Class"
  type        = string 
  default     = "all"
}

variable "nomad_namespace" {
  description = "Nomad Name Space"
  type        = string 
  default     = "sys"
}

variable "nomad_region" {
  description = "Nomad Region"
  type        = string
  default     = ""
}

variable "extra_hosts" {
  description = "Extrahosts"
  type        = string
  default     = ""
}

variable "consul_token" {
  description = "Consul Token"
  type        = string
}

variable "tm_metrics_enabled" {
  description = "Are metrics enabled?"
  type        = bool
  default     = false
}

variable "tm_logs_enabled" {
  description = "Are logs enabled?"
  type        = bool
  default     = false
}

# Registry 

variable "registry_username" {
  description = "Registry User Name"
  type        = string
  default     = "NULL"  
}
variable "registry"          {
  description = "Registry Name"
  type        = string
  default     = "NULL" 
}
variable "registry_password"    {
  description = "Registry Token"
  type        = string
  default     = "NULL" 
}

# Metrics 

variable "tm_metrics_name" {
  description = "Teramonitor Metrics service name"
  default     = "tm-metrics"
}

variable "tm_metrics_image" {
  description = "Docker image for Teramonitor Metrics service"
  default     = "telegraf:1.31.2-alpine"
}

variable "tm_metrics_image_custom" {
  description = "Docker image for Teramonitor Metrics service"
  default     = ""
}

variable "tm_metrics_custom_config" {
  description = "Teramonitor metrics custom configuration?"
  type        = bool
  default     = false
}

variable "tm_metrics_config" {
  description = "Teramonitor metrics configuration"
  default     = null
}

variable "tm_metrics_statsd_port" {
  description = "Teramonitor metrics StatsD port"
  type        = number
  default     = 8125
}

variable "tm_metrics_tsdb_type" {
  description = "TSDB Type (Only supported for now influxdb)"
  type        = string
  default     = "influxdb"
}

variable "tm_metrics_tsdb_schema" {
  description = "TSDB Schema"
  type        = string
  default     = "https"
}

variable "tm_metrics_tsdb_host" {
  description = "TSDB Host"
  type        = string
  default     = ""
}

variable "tm_metrics_tsdb_port" {
  description = "TSDB Port"
  type        = number
  default     = 8086
}

variable "tm_metrics_tsdb_dbname" {
  description = "TSDB Database Name"
  type        = string
  default     = ""
}

variable "tm_metrics_tsdb_username" {
  description = "TSDB Username"
  type        = string
}

variable "tm_metrics_tsdb_password" {
  description = "TSDB Password"
  type        = string
}

variable "tm_metrics_input_interval" {
  description = "TM metrics input interval"
  type        = string
  default     = "30s"
}

variable "tm_metrics_flush_interval" {
  description = "TM metrics flush interval"
  type        = string
  default     = "30s"
}

variable "tm_metrics_collection_jitter" {
  description = "TM metrics collection jitter"
  type        = string
  default     = "0s"
}

variable "tm_metrics_flush_jitter" {
  description = "TM metrics flush jitter"
  type        = string
  default     = "0s"
}

variable "tm_metrics_cpu" {
  description = "CPU to assign to Teramonitor Metrics Service"
  type        = number
  default     = 100
}

variable "tm_metrics_ram" {
  description = "RAM to assign to Teramonitor Metrics Service"
  type        = number
  default     = 300
}

# Logs

variable "tm_logs_name" {
  description = "Teramonitor Logs service name"
  type        = string
  default     = "tm-logs"
}

variable "tm_logs_image" {
  description = "Docker image for Teramonitor Logs service"
  type        = string
  default     = "timberio/vector:0.33.0-debian"
}

variable "tm_logs_custom_config" {
  description = "Teramonitor Logs custom configuration?"
  type        = bool
  default     = false
}

variable "tm_logs_config" {
  description = "Teramonitor Logs configuration"
  default     = null
}

variable "tm_logs_url" {
  description = "TM Logs Url"
  type        = string
  default     = ""
}

variable "tm_logs_username" {
  description = "TM Logs username"
  type        = string
  default     = ""
}

variable "tm_logs_password" {
  description = "TM Logs password"
  type        = string
  default     = ""
}

variable "tm_logs_infra_enabled" {
  description = "TM Logs for infrastructure(nomad/consul) enabled?"
  type        = bool 
  default     = false
}

variable "tm_logs_cpu" {
  description = "CPU to assign to Teramonitor Logs Service"
  type        = number
  default     = 100
}

variable "tm_logs_ram" {
  description = "RAM to assign to Teramonitor Logs Service"
  type        = number
  default     = 300
}

variable "tm_metrics_servers_conf" {
  description = "TM Metrics for servers."
  type        = string
  default     = "NULL"
}

variable "tm_batch_max_bytes" {
  description = "The maximum size of a batch that is processed by a sink."
  type = number
  default = 25000000
}
variable "tm_request_timeout_secs" {
  description = "The time a request can take before being aborted"
  type = number
  default = 180
}
variable "tm_request_rate_limit_duration_secs" {
  description = "The time window used for the rate_limit_num (default: 9.223372036854776e+18) option."
  type = number
  default = 5
}
variable "tm_buffer_max_events" {
  description = "The maximum number of events allowed in the buffer."
  type = number
  default = 15000
}

variable "tm_span" {
  description = "Span to check healthcheck in minutes."
  type = number
  default = 5
}

variable "tm_error_count_rate" {
  description = "Number of error needed to trigger to be considered as faulty service."
  type = number
  default = 10  
}

variable "plugins" {
  description = "List of Nomad Plugins"
  type        = list
  default     = []
}

variable "ca_certificate" {
  description = "CA Certificate"
  type        = string
}

variable "certificate" {
  description = "Certificate"
  type        = string
}

variable "key" {
  description = "Key"
  type        = string
}

variable "custom_metrics" {
  description = "Additional metrics"
  default     = {}
}

variable "system_excluded_containers" {
  description = "System excluded containers"
  type        = list
  default     = [
    "connect-proxy-",
    "tm-metrics",
    "tm-logs",
  ]
}

variable "excluded_containers" {
  description = "Exclude logs for containers"
  type        = list
  default     = []
}

variable "memlock" {
  description = "Telegraf job memlock value"
  default = "114688"
}

variable "consul_ca_custom_path" {
  description = "Consul ca custom path"
  type        = string 
  default     = ""
}

variable "consul_crt_custom_path" {
  description = "Consul crt custom path"
  type        = string 
  default     = ""
}

variable "consul_key_custom_path" {
  description = "Consul key custom path"
  type        = string 
  default     = ""
}