###############################
#    Teramonitor - Metrics    #
###############################

locals {
  metrics = {
    "cpu" = [
      "usage_idle",
      "usage_iowait",
      "usage_system",
      "usage_user"
    ]
    "diskio" = [
      "read_bytes",
      "write_bytes"
    ]
    "disk" = [
      "total",
      "used_percent"
    ]
    "netstat" = [
      "tcp_established",
      "tcp_syn_recv",
      "tcp_time_wait",
      "udp_socket"
    ]
    "net" = [
      "bytes_recv",
      "bytes_sent"
    ]
    "kernel" = []
    "mem" = [
      "available_percent",
      "buffered",
      "cached",
      "free",
      "total",
      "used",
      "used_percent"
    ]
    "processes" = [
      "blocked",
      "running",
      "sleeping",
      "total",
      "total_threads",
      "zombies"
    ]
    "swap" = []
    "system" = [
      "load1",
      "load15",
      "load5",
      "n_cpus",
      "uptime"
    ]
    "nomad-prometheus" = [
      "nomad_client_allocs_oom_killed",
      "nomad_client_unallocated_cpu",
      "nomad_client_unallocated_disk",
      "nomad_client_unallocated_memory",
      "nomad_nomad_broker_total_ready",
      "nomad_nomad_heartbeat_active",
      "nomad_nomad_plan_queue_depth",
      "nomad_nomad_rpc_request",
      "nomad_runtime_alloc_bytes",
      "nomad_runtime_heap_objects",
      "nomad_runtime_num_goroutines"
    ]
    "consul-prometheus" = [
      "consul_autopilot_healthy",
      "consul_raft_apply",
      "consul_raft_state_candidate",
      "consul_raft_state_leader",
      "consul_rpc_request",
      "consul_runtime_alloc_bytes",
      "consul_runtime_num_goroutines",
      "consul_runtime_sys_bytes",
      "consul_runtime_total_gc_pause_ns"
    ]
  }
}

# Resources
resource "nomad_job" "tm_metrics" {
  count                   = var.tm_metrics_enabled ? 1 : 0
  jobspec                 = templatefile("${path.module}/templates/sys-tm-metrics.tpl",
  {
    nomad_datacenter             = var.nomad_datacenter
    env                          = var.env
    namespace                    = var.nomad_namespace
    node_class                   = var.nomad_node_class

    registry                     = var.registry
    registry_username            = var.registry_username
    registry_password            = var.registry_password

    service_image                = var.tm_metrics_image
    service_name                 = var.tm_metrics_name
    extra_hosts                  = var.extra_hosts
    error_count_rate             = var.tm_error_count_rate
    health_check                 = file("${path.module}/files/check_telegraf_status.sh")
    span                         = var.tm_span
    
    tm_metrics_connection_conf   = templatefile("${path.module}/templates/connection_metrics_config.conf.tpl", {
      env                        = var.env
      datacenter                 = var.nomad_datacenter
      role                       = var.nomad_node_class
      tm_metrics_tsdb_type       = var.tm_metrics_tsdb_type
      consul_token               = var.consul_token
    })

    tm_metrics_conf              = templatefile("${path.module}/templates/default_metrics_config.conf.tpl",{
      cpu_metrics               = "\"${join("\",\"",concat(lookup(local.metrics, "cpu", []),lookup(var.custom_metrics, "cpu", [])))}\""
      diskio_metrics            = "\"${join("\",\"",concat(lookup(local.metrics, "diskio", []),lookup(var.custom_metrics, "diskio", [])))}\""
      disk_metrics              = "\"${join("\",\"",concat(lookup(local.metrics, "disk", []),lookup(var.custom_metrics, "disk", [])))}\""
      netstat_metrics           = "\"${join("\",\"",concat(lookup(local.metrics, "netstat", []),lookup(var.custom_metrics, "netstat", [])))}\""
      net_metrics               = "\"${join("\",\"",concat(lookup(local.metrics, "net", []),lookup(var.custom_metrics, "net", [])))}\""
      kernel_metrics            = "\"${join("\",\"",concat(lookup(local.metrics, "kernel", []),lookup(var.custom_metrics, "kernel", [])))}\""
      mem_metrics               = "\"${join("\",\"",concat(lookup(local.metrics, "mem", []),lookup(var.custom_metrics, "mem", [])))}\""
      processes_metrics         = "\"${join("\",\"",concat(lookup(local.metrics, "processes", []),lookup(var.custom_metrics, "processes", [])))}\""
      swap_metrics              = "\"${join("\",\"",concat(lookup(local.metrics, "swap", []),lookup(var.custom_metrics, "swap", [])))}\""
      system_metrics            = "\"${join("\",\"",concat(lookup(local.metrics, "system", []),lookup(var.custom_metrics, "system", [])))}\""
      nomad_prometheus_metrics  = "\"${join("\",\"",concat(lookup(local.metrics, "nomad-prometheus", []),lookup(var.custom_metrics, "nomad-prometheus", [])))}\""
      consul_ca_path            = var.consul_ca_custom_path == "" ? "/opt/consul/tls/ca/ca-chain.cert.pem" : var.consul_ca_custom_path 
      consul_crt_path           = var.consul_crt_custom_path == "" ? "/opt/consul/tls/consul.cert.pem" : var.consul_crt_custom_path  
      consul_key_path           = var.consul_key_custom_path == "" ? "/opt/consul/tls/consul.key.pem" : var.consul_key_custom_path  
      consul_prometheus_metrics = "\"${join("\",\"",concat(lookup(local.metrics, "consul-prometheus", []),lookup(var.custom_metrics, "consul-prometheus", [])))}\""
      consul_token              = var.consul_token
      plugins                   = var.plugins
    })
    
    # Environment variables
    tm_metrics_statsd_port       = var.tm_metrics_statsd_port
    tm_metrics_tsdb_type         = var.tm_metrics_tsdb_type
    tm_metrics_tsdb_schema       = var.tm_metrics_tsdb_schema
    tm_metrics_tsdb_host         = var.tm_metrics_tsdb_type == "influxdb" ? "${var.tm_metrics_tsdb_schema}://${var.tm_metrics_tsdb_host}:${var.tm_metrics_tsdb_port}" : var.tm_metrics_tsdb_host
    tm_metrics_tsdb_port         = var.tm_metrics_tsdb_port
    tm_metrics_tsdb_dbname       = var.tm_metrics_tsdb_dbname
    tm_metrics_tsdb_username     = var.tm_metrics_tsdb_username
    tm_metrics_tsdb_password     = var.tm_metrics_tsdb_password
    tm_metrics_input_interval    = var.tm_metrics_input_interval
    tm_metrics_flush_interval    = var.tm_metrics_flush_interval
    tm_metrics_collection_jitter = var.tm_metrics_collection_jitter
    tm_metrics_flush_jitter      = var.tm_metrics_flush_jitter
    
    # Additional configuration
    consul_token                 = var.consul_token
    consul_tags                  = "\"tm-metrics\",\"${var.env}\""
    res_cpu                      = var.tm_metrics_cpu
    res_ram                      = var.tm_metrics_ram
  })
  deregister_on_id_change = "true"

  lifecycle {
    create_before_destroy = false
  }
}

resource "nomad_job" "tm_metrics_servers" {
  count = var.tm_metrics_enabled ? var.tm_metrics_servers_conf != "NULL" ? 1 : 0  : 0

  jobspec = templatefile("${path.module}/templates/monitor_servers.tpl",
  {
    nomad_datacenter             = var.nomad_datacenter
    env                          = var.env
    namespace                    = "sys"
    node_class                   = var.nomad_node_class
    service_image                = var.tm_metrics_image
    service_name                 = "${var.tm_metrics_name}-telegraf"
    tm_metrics_conf              = var.tm_metrics_servers_conf
    tm_metrics_connection_conf   = templatefile("${path.module}/templates/connection_metrics_config.conf.tpl", {
      env                        = var.env
      datacenter                 = var.nomad_datacenter
      role                       = var.nomad_node_class
      tm_metrics_tsdb_type       = var.tm_metrics_tsdb_type
      consul_token               = var.consul_token
    })
    memlock                      = var.memlock

    # Environment variables
    tm_metrics_statsd_port       = var.tm_metrics_statsd_port
    tm_metrics_tsdb_type         = var.tm_metrics_tsdb_type
    tm_metrics_tsdb_schema       = var.tm_metrics_tsdb_schema
    tm_metrics_tsdb_host         = var.tm_metrics_tsdb_type == "influxdb" ? "${var.tm_metrics_tsdb_schema}://${var.tm_metrics_tsdb_host}:${var.tm_metrics_tsdb_port}" : var.tm_metrics_tsdb_host
    tm_metrics_tsdb_port         = var.tm_metrics_tsdb_port
    tm_metrics_tsdb_dbname       = var.tm_metrics_tsdb_dbname
    tm_metrics_tsdb_username     = var.tm_metrics_tsdb_username
    tm_metrics_tsdb_password     = var.tm_metrics_tsdb_password
    tm_metrics_input_interval    = var.tm_metrics_input_interval
    tm_metrics_flush_interval    = var.tm_metrics_flush_interval
    tm_metrics_collection_jitter = var.tm_metrics_collection_jitter
    tm_metrics_flush_jitter      = var.tm_metrics_flush_jitter
    ca_certificate               = var.ca_certificate
    certificate                  = var.certificate
    key                          = var.key
    consul_tags                  = "\"tm-metrics\",\"${var.env}\""

    cpu = var.tm_metrics_cpu
    ram = var.tm_metrics_ram
  })
  lifecycle {
    create_before_destroy = false
  }
}