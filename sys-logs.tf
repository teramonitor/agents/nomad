############################
#    Teramonitor - Logs    #
############################

locals {
  excluded_containers = jsonencode(flatten(
    concat(
      var.system_excluded_containers,
      var.excluded_containers,
    )
  ))
}

# Resources
resource "nomad_job" "tm_logs" {
  count                          = var.tm_logs_enabled ? 1 : 0
  jobspec                        = templatefile("${path.module}/templates/sys-tm-logs.tpl",
  {
    nomad_datacenter             = var.nomad_datacenter
    env                          = var.env
    namespace                    = var.nomad_namespace
    node_class                   = var.nomad_node_class
    service_image                = var.tm_logs_image
    service_name                 = var.tm_logs_name
    extra_hosts                  = var.extra_hosts

    tm_logs_conf                 = var.tm_logs_custom_config ? templatefile("${path.cwd}/templates/custom_logs_config.conf.tpl",
    {
      datacenter       = var.env
      tm_logs_url      = var.tm_logs_url
      tm_logs_username = var.tm_logs_username
      tm_logs_password = var.tm_logs_password
    }) : templatefile("${path.module}/templates/default_logs_config.toml.tpl", 
    {
      datacenter                       = var.env
      tm_logs_url                      = var.tm_logs_url
      tm_logs_username                 = var.tm_logs_username
      tm_logs_password                 = var.tm_logs_password
      tm_logs_infra_enabled            = var.tm_logs_infra_enabled
      excluded_containers              = local.excluded_containers
      batch_max_bytes                  = var.tm_batch_max_bytes
      request_timeout_secs             = var.tm_request_timeout_secs
      request_rate_limit_duration_secs = var.tm_request_rate_limit_duration_secs
      buffer_max_events                = var.tm_buffer_max_events
    })

    # Environment variables
    tm_logs_url                  = var.tm_logs_url
    tm_logs_username             = var.tm_logs_username
    tm_logs_password             = var.tm_logs_password
    tm_logs_infra_enabled        = var.tm_logs_infra_enabled

    # Additional configuration
    consul_token                 = var.consul_token
    consul_tags                  = "\"tm-logs\",\"${var.env}\""
    res_cpu                      = var.tm_logs_cpu
    res_ram                      = var.tm_logs_ram
  })
  deregister_on_id_change = "true"

  lifecycle {
    create_before_destroy = false
  }
}