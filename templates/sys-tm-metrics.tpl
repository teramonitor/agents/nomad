job "${env}-${service_name}" {
  datacenters = ["${nomad_datacenter}"]
  namespace   = "${namespace}"
  type        = "system"
  priority    = 100  

  %{ if node_class != "all" }
  constraint {
    attribute = "$${node.class}"
    operator  = "regexp"
    value     = "${node_class}.*"
  }
  %{endif}

  group "${service_name}" {
    restart {
      interval = "1m"
      attempts = 4
      delay    = "15s"
      mode     = "delay"
    }
    network { 
      port "statsd" {
        static = 8125
        to     = 8215
      }
    }
    task "${service_name}" {
      driver = "docker"

      config {
        privileged = true
        network_mode = "host"

        image = "${service_image}"
        %{ if registry != "NULL" }        
        auth {
          username       = "${registry_username}"
          password       = "${registry_password}"
          server_address = "${registry}"
        }
        %{endif}
        entrypoint = [
          "telegraf",
          "--config",
          "/local/${service_name}/telegraf.conf"
        ]

        %{ if extra_hosts != "" }
        extra_hosts = "${extra_hosts}"
        %{endif}

        volumes = [
          "/sys:/sys",
          "/proc:/proc",
          "/var/run/utmp:/var/run/utmp",
          "/opt/consul/tls:/opt/consul/tls",
          "/opt/nomad/data/client/csi:/opt/nomad/data/client/csi"
        ]
      }

      env {
        TSDB_TYPE         = "${tm_metrics_tsdb_type}"
        TSDB_HOST         = "${tm_metrics_tsdb_host}"
        TSDB_DATABASE     = "${tm_metrics_tsdb_dbname}"
        TSDB_USERNAME     = "${tm_metrics_tsdb_username}"
        TSDB_PASSWORD     = "${tm_metrics_tsdb_password}"
        INPUT_INTERVAL    = "${tm_metrics_input_interval}"
        FLUSH_INTERVAL    = "${tm_metrics_flush_interval}"
        COLLECTION_JITTER = "${tm_metrics_collection_jitter}" 
        FLUSH_JITTER      = "${tm_metrics_flush_jitter}"
        STATSD_PORT       = "${tm_metrics_statsd_port}" 
        HOSTNAME          = "$${attr.unique.hostname}"
        ERROR_COUNT_RATE  = "${error_count_rate}"
        SPAN              = "${span}"
      }

      template {
        data        = <<EOH
${tm_metrics_connection_conf}
${tm_metrics_conf}
EOH
        destination = "local/${service_name}/telegraf.conf"
      }
%{ if health_check != "" }
      template {
        data        = <<EOH
${health_check}
EOH
        destination = "local/check_status.sh"
        perms = 700
      }
      service {
        name = "${env}-${service_name}"
        check {
          type     = "script"
          name     = "log errors"
          command  = "/local/check_status.sh"
          interval = "300s"
          timeout  = "20s"

          check_restart {
            limit = 1
            grace = "600s"
            ignore_warnings = false
          }
        }
      }
%{ endif }
      resources {
        cpu    = ${res_cpu} 
        memory = ${res_ram}
      }
    }
  }
}