[[inputs.cpu]]
  percpu           = true
  totalcpu         = true
  collect_cpu_time = false
  report_active    = false
  fieldinclude = [${cpu_metrics}]
[[inputs.diskio]]
  fieldinclude = [${diskio_metrics}]
[[inputs.disk]]
  fieldinclude = [${disk_metrics}]
  ignore_fs = ["tmpfs", "devtmpfs", "devfs", "iso9660", "overlay", "aufs", "squashfs"]
%{ for plugin in plugins ~}
[[processors.strings]]
  [[processors.strings.trim_prefix]]
    tag = "path"
    prefix = "/opt/nomad/data/client/csi/node/${plugin}"
  [[processors.strings.trim_suffix]]
    tag = "path"
    suffix = "/rw-file-system-single-node-writer"
%{ endfor ~}
[[inputs.netstat]]
  fieldinclude = [${netstat_metrics}]
[[inputs.net]]
  fieldinclude = [${net_metrics}]
[[inputs.kernel]]
  fieldinclude = [${kernel_metrics}]
[[inputs.mem]]
  fieldinclude = [${mem_metrics}]
[[inputs.processes]]
  fieldinclude = [${processes_metrics}]
[[inputs.swap]]
  fieldinclude = [${swap_metrics}]
[[inputs.system]]
  fieldinclude = [${system_metrics}]
[[inputs.prometheus]]
  urls = ["https://$NOMAD_IP_statsd:4646/v1/metrics?format=prometheus"]
  insecure_skip_verify = true
  tls_ca = "${consul_ca_path}"
  tls_cert = "${consul_crt_path}"
  tls_key = "${consul_key_path}"
  namepass = [${nomad_prometheus_metrics}]
[[inputs.prometheus]]
  urls = ["http://$NOMAD_IP_statsd:8501/v1/agent/metrics?format=prometheus"]
  bearer_token_string = "${consul_token}"
  fieldinclude = [${consul_prometheus_metrics}]
[[inputs.consul]]
  address = "localhost:8501"
  scheme = "http"
  token = "${consul_token}"
  metric_version = 2
[[inputs.statsd]]
  protocol                 = "udp"
  max_tcp_connections      = 250
  tcp_keep_alive           = false
  service_address          = ":8125"
  delete_gauges            = true
  delete_counters          = true
  delete_sets              = true
  delete_timings           = true
  percentiles              = [90]
  metric_separator         = "_"
  datadog_extensions       = true
  allowed_pending_messages = 10000
  percentile_limit         = 1000 