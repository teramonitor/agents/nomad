job "${env}-${service_name}" {
  datacenters = ["${nomad_datacenter}"]
  namespace   = "${namespace}"
  type = "system"
  priority = 95
  %{ if node_class != "all" }
  constraint {
    attribute = "$${node.class}"
    operator  = "regexp"
    value     = "${node_class}.*"
  }
  %{endif}
  group "${service_name}" {
    restart {
      interval = "1m"
      attempts = 4
      delay    = "15s"
      mode     = "delay"
    }
    task "${service_name}" {
      driver = "docker"
      config {
        image = "${service_image}"
        args = [
          "-c", 
          "local/${service_name}/vector.conf"
        ]
        %{ if extra_hosts != "" }
        extra_hosts = "${extra_hosts}"
        %{endif}  
        volumes = [
          "/var/lib/docker/containers:/var/lib/docker/containers",
          "/var/run/docker.sock:/var/run/docker.sock",
          "/etc/docker/tls:/etc/docker/tls",
          "/var/lib/vector/:/var/lib/vector/",
          %{ if tm_logs_infra_enabled == true }
          "/opt/consul/log/:/opt/consul/",
          "/opt/nomad/log/:/opt/nomad/"
          %{endif}
        ]
        privileged = true
      }
      env {
        DOCKER_CERT_PATH="/etc/docker/tls"
        DOCKER_HOST="unix:///var/run/docker.sock"
      }
      template {
        data        = <<EOH
${tm_logs_conf}
EOH
        left_delimiter = "{{{"
        destination = "local/${service_name}/vector.conf"
      }
      resources {
        cpu    = ${res_cpu} 
        memory = ${res_ram}
      }
    }
  }
}