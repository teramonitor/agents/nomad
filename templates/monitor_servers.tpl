job "${env}-${service_name}" {
    datacenters = ["${nomad_datacenter}"]
    namespace   = "${namespace}"
    type = "service"
    priority = 30 
    constraint {
      attribute = "$${node.class}"
      operator  = "="
      value     = "${node_class}"
    }
    group "${service_name}" {
      reschedule {
        delay          = "300s"
        interval       = "72h"
        delay_function = "constant"
        unlimited      = true
      }
      restart {
        attempts = 3
        delay    = "300s"
      }
      task "telegraf" {
        driver = "docker"
        config {
          image = "${service_image}"
          force_pull = false
          entrypoint = [
            "telegraf",
            "--config",
            "/local/telegraf.conf"
          ]
          ulimit {
            memlock = ${memlock}
          }
        }
        env {
          TSDB_TYPE         = "${tm_metrics_tsdb_type}"
          TSDB_HOST         = "${tm_metrics_tsdb_host}"
          TSDB_DATABASE     = "${tm_metrics_tsdb_dbname}"
          TSDB_USERNAME     = "${tm_metrics_tsdb_username}"
          TSDB_PASSWORD     = "${tm_metrics_tsdb_password}"
          INPUT_INTERVAL    = "${tm_metrics_input_interval}"
          FLUSH_INTERVAL    = "${tm_metrics_flush_interval}"
          COLLECTION_JITTER = "${tm_metrics_collection_jitter}" 
          FLUSH_JITTER      = "${tm_metrics_flush_jitter}"
          STATSD_PORT       = "${tm_metrics_statsd_port}" 
          HOSTNAME          = "$${attr.unique.hostname}"
        }
        template {
          data = <<EOF
${tm_metrics_connection_conf}
${tm_metrics_conf}
EOF
          destination = "local/telegraf.conf"
        }
      template {
        data        = <<EOH
${ca_certificate}
EOH
        destination = "local/ca.pem"
      }
      template {
        data        = <<EOH
${certificate}
EOH
        destination = "local/cert.pem"
      }
      template {
        data        = <<EOH
${key}
EOH
        destination = "local/key.pem"
      }
        resources {
          cpu = ${cpu}
          memory = ${ram}
          network {
            port "tcp" {}
          }
        }
      }
    }
  }
