[global_tags]
  env        = "${env}"
  datacenter = "${datacenter}"
  role       = "${role}"
[agent]
  hostname            = "$HOSTNAME"
  interval            = "$INPUT_INTERVAL"
  round_interval      = true
  metric_batch_size   = 1000
  metric_buffer_limit = 10000
  collection_jitter   = "$COLLECTION_JITTER"
  flush_interval      = "$FLUSH_INTERVAL"
  flush_jitter        = "$FLUSH_JITTER"
  precision           = ""
  debug               = false
  quiet               = false
  logfile             = ""
%{ if "${tm_metrics_tsdb_type}" == "influxdb" }
[[outputs.influxdb]]
  urls                   = ["$TSDB_HOST"]
  database               = "$TSDB_DATABASE"
  username               = "$TSDB_USERNAME"
  password               = "$TSDB_PASSWORD"
  skip_database_creation = true
%{ endif }
%{ if "${tm_metrics_tsdb_type}" == "prometheus" }
[[outputs.http]]
  url         = "$TSDB_HOST/api/v1/push"
  method      = "POST"
  data_format = "prometheusremotewrite"
  username    = "$TSDB_USERNAME"
  password    = "$TSDB_PASSWORD"
  [outputs.http.headers]
    Content-Type = "application/x-protobuf"
    Content-Encoding = "snappy"
    X-Prometheus-Remote-Write-Version = "0.1.0"
%{ endif }
