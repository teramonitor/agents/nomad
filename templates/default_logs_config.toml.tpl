data_dir = "/var/lib/vector"

[sources.docker]
    type = "docker_logs"
    exclude_containers = ${excluded_containers}

    [sources.docker.multiline]
    start_pattern = "^[A-Z]{1}[a-z]{2}\\s\\d{2}\\D\\s\\d{4}|[\\d]{4}-\\d{2}-\\d{2}|[\\d]{2}:\\d{2}:\\d{2}\\.\\d{3}"
    mode = "halt_before"
    condition_pattern = "^[A-Z]{1}[a-z]{2}\\s\\d{2}\\D\\s\\d{4}|[\\d]{4}-\\d{2}-\\d{2}|[\\d]{2}:\\d{2}:\\d{2}\\.\\d{3}"
    timeout_ms = 1000

[transforms.service_name]
type = "remap"
inputs = ["*"]
source = '''
service_name = replace(.container_name, r'-[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$', "") ?? ""
.service_name = service_name
del(.label)
del(.container_name)
del(.container_id)
'''
%{ if tm_logs_infra_enabled == true }
[sources.infra_logs]
type = "file"
include = [
  "/opt/consul/*.log",
  "/opt/nomad/*.log"
]
%{endif}

[sinks.loki]
type = "loki"
healthcheck.enabled = false
compression = "gzip"
encoding.codec = "json"
endpoint = "${tm_logs_url}"
auth.strategy = "basic"
auth.user = "${tm_logs_username}"
auth.password = "${tm_logs_password}"
inputs = [ "service_name" ]
labels.env = "${datacenter}"
labels.service_name = "{{ .service_name }}"
labels.source = "{{ .source_type }}"
labels.output = "{{ .stream }}"
out_of_order_action = "rewrite_timestamp"
batch.max_bytes = ${batch_max_bytes}
request.timeout_secs = ${request_timeout_secs}
request.rate_limit_duration_secs = ${request_rate_limit_duration_secs}
buffer.max_events = ${buffer_max_events}