#!/bin/sh
set -x
# Sript configuration
error_exit_code=2

access_log="/alloc/logs/${NOMAD_TASK_NAME}.stderr.*"
error_message="Error writing to output"
metric="tm_error"

if ! command -v nc &> /dev/null;then
    echo "nc could not be found. Proceeding to install it..."
    apt update &> /dev/null
    apt install netcat -y &> /dev/null
fi

if [ -z $STATSD_SERVER ];then
    export STATSD_SERVER="localhost"
fi

if [ -z $ERROR_COUNT_RATE ];then
    export ERROR_COUNT_RATE=2
fi

if [ -z $SPAN ];then
    export SPAN=5 # minutes
fi

# Functions
function report_to_teramonitor() {
    COUNT=$1
    REASON=$2
    echo "----> Reporting result to TeraMonitor: $MESSAGE"
    echo -e "${NOMAD_TASK_NAME}_${metric}_reason:$REASON|g\n${NOMAD_TASK_NAME}_${metric}_count:$COUNT|g" | nc -C -w 1 -u $STATSD_SERVER 8125
    echo " ----> Reporting to teramonitor completed."
}

function exit_on_error() {
    case $2 in
      1)
        MESSAGE="Error sending metrics output."
      ;;
      *)
        MESSAGE="Error telegraf."
      ;;
    esac
    echo "** ERROR: $MESSAGE"
    report_to_teramonitor $1 $2;
    exit $error_exit_code
}

# Initialization
error_count=0
current_date=$(date +%s)
for i in $(seq 1 $SPAN); do 
    error_count=$(($error_count+$(grep "$(date +"%Y-%m-%dT%H:%M" --date=@$(($current_date-$i*60)))" $access_log | grep "$error_message" | wc -l)))
done
if [ "$error_count" -gt "$ERROR_COUNT_RATE" ]; then
    exit_on_error "$error_count" 1 
fi

exit 0